#include <stdio.h>
#include "asss.h"

static Imodman		*mm;
static Igame		*game;

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	Target tgt;
	tgt.type = T_PLAYER;
	tgt.u.p = p;
	if (newfreq != oldfreq)
	{
		game->GivePrize(&tgt, PRIZE_WARP, 1);
	}
}


EXPORT const char info_freqchangewarp[] = "FreqChangeWarp by JoWie ("BUILDDATE")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(game);
}

EXPORT int MM_freqchangewarp(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm      = mm_;
                game    = mm->GetInterface(I_GAME        , ALLARENAS);


                if (!game)
                {
                        ReleaseInterfaces();
                        printf("<freqchangewarp> Missing interfaces");
                        return MM_FAIL;
                }

                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
                ReleaseInterfaces();

                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);

                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);

                return MM_OK;
        }

        return MM_FAIL;
}

